import React, { Component } from "react";
import Container from "./containers/index";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Container className="container" />
      </div>
    );
  }
}
export default App;
