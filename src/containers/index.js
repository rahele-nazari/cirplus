import React from "react";
import { connect } from "react-redux";
import { fetchEmitionData } from "../store/action/index";
import FusionCharts from "fusioncharts";
import ReactFC from "react-fusioncharts";
import TimeSeries from "fusioncharts/fusioncharts.timeseries";
import "./style.css";

ReactFC.fcRoot(FusionCharts, TimeSeries);
const dataSource = {
  chart: {},
  caption: {
    text: "Co2 emition"
  },
  subcaption: {
    text: "emition data points"
  },
  yaxis: [
    {
      plot: [
        {
          value: "Closing Price",
          connectnulldata: true,
          aggregation: "avg",
          type: "line"
        }
      ],
      format: {
        prefix: ""
      },
      title: "emition"
    }
  ],
  xaxis: [
    {
      plot: [
        {
          value: "Time",
          aggregation: "avg",
          type: "line"
        }
      ]
    }
  ],
  legend: {},
  plotconfig: {},
  extensions: {}
};
class Container extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      timeseriesDs: {
        type: "timeseries",
        renderAt: "container",
        width: "600",
        height: "400",
        dataSource
      }
    };
  }
  componentDidUpdate(prevProp) {
    if (!prevProp.isSuccess & this.props.isSuccess) {
      const emition = this.props.emitionData.data;
      var newEmitionStore = [];
      for (var i = 0; i < emition.length; i++) {
        newEmitionStore.push([emition[i]["date"], emition[i]["value"]]);
      }
      console.log("newEmitionStore", newEmitionStore);
      const data = newEmitionStore;
      const schema = [
        {
          name: "Time",
          type: "date",
          format: "%Y-%m-%d"
        },
        {
          name: "Emition Value",
          type: "number"
        }
      ];
      const fusionTable = new FusionCharts.DataStore().createDataTable(
        data,
        schema
      );
      const timeseriesDs = Object.assign({}, this.state.timeseriesDs);
      timeseriesDs.dataSource.data = fusionTable;
      this.setState({
        timeseriesDs
      });
    }
  }
  componentDidMount() {
    this.props.fetchData();
  }
  render() {
    console.log(this.state.timeseriesDs);
    return this.props.emitionData ? (
      <ReactFC className="chart" {...this.state.timeseriesDs} />
    ) : (
      <div className="chart loading">
        <span>loading ...</span>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    emitionData: state.emition,
    isSuccess: state.isSuccess
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchData: () => {
      dispatch(fetchEmitionData());
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Container);
