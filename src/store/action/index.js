import {
  FETCHING_EMITION_DATA,
  SUCCESS_EMITION_DATA,
  FAILED_EMITION_DATA
} from "./types";

export function fetchEmitionData() {
  return {
    type: FETCHING_EMITION_DATA
  };
}
export function successEmitionData(data) {
  return {
    type: SUCCESS_EMITION_DATA,
    data
  };
}
export function failedEmitionData() {
  return {
    type: FAILED_EMITION_DATA
  };
}
