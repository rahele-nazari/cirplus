import { put, takeEvery, call } from "redux-saga/effects";
import { successEmitionData, failedEmitionData } from "./index";
import axios from "axios";

axios
  .get()
  .then(function(response) {})
  .catch(function(error) {})
  .then(function() {});

export function* getData() {
  try {
    const response = yield call(
      axios.get,
      "https://pkgstore.datahub.io/core/co2-ppm-daily/co2-ppm-daily_json/data/5918112417b2a061cfca919b581a3eb0/co2-ppm-daily_json.json ",
      {}
    );
    yield put(successEmitionData(response));
  } catch (error) {
    yield put(failedEmitionData());
  }
}

export function* watchIncrementAsync() {
  yield takeEvery("FETCHING_EMITION_DATA", getData);
}
