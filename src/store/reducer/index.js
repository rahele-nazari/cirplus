import {
  FETCHING_EMITION_DATA,
  SUCCESS_EMITION_DATA,
  FAILED_EMITION_DATA
} from "../action/types";

function reducer(state = {}, action) {
  switch (action.type) {
    case FETCHING_EMITION_DATA:
      return {
        ...state,
        isFetching: true,
        isSuccess: false,
        isFailed: false
      };
    case SUCCESS_EMITION_DATA:
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
        isFailed: false,
        emition: action.data
      };
    case FAILED_EMITION_DATA:
      return {
        ...state,
        isFetching: false,
        isSuccess: false,
        isFailed: true
      };
    default:
      return state;
  }
}

export default reducer;
